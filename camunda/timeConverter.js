// Converter for timers
function MinutesToDuration(s) {
    var days = Math.floor(s / 1440);
    s = s - days * 1440;
    var hours = Math.floor(s / 60);
    s = s - hours * 60;

    var dur = "PT";
    if (days > 0) {dur += days + "D"};
    if (hours > 0) {dur += hours + "H"};
    dur += s + "M"

    return dur;
}

console.log(MinutesToDuration("0"));
console.log(MinutesToDuration("10"));
console.log(MinutesToDuration("90"));
console.log(MinutesToDuration(1000));
console.log(MinutesToDuration(10000));