const TASKS = {
    SUBMIT_REQUEST: {                                         // Need subscribe
        name: 'Submit request',
        topic: 'submit_request',
        type: 'service task',
    },
    ACCEPT_USERS_REQUEST_BY_LAWYER: {
        name: 'Accept users request by lawyer',
        assignee: 'lawyer',
        type: 'user task',
    } ,
    LAWYER_REVIEW_STAGE: {
        name: 'Lawyer review stage',
        assignee: 'lawyer',
        type: 'user task',
        expressions: {
            accept: 'accept_request',
            decline: 'reject_request',
        }
    },
    LAWYER_WORK_WITH_REQUEST: {
        name: 'Lawyer work with request',
        assignee: 'lawyer',
        type: 'user task',
        expressions: {
            contact: 'contact_user',
            comment: 'lawyer_added_comment',
        }
    },
    RESOLVE_USER_REQUEST: {
        name: 'Resolve user request',
        assignee: 'lawyer',
        type: 'user task',
    },
    LAWYER_ADD_COMMENT: {
        name: 'Lawyer adds a comment',
        assignee: 'lawyer',
        type: 'user task',
    },
    NOTIFY_USER_ABOUT_STATUS_CHANGE: {                          // Need subscribe
        name: 'Notify user about status change',
        topic: 'notify_user_about_status_change',
        type: 'service task',
    },
    USER_REVIEWS_DOCUMENT: {
        name: 'User reviews document',
        assignee: 'lawyer',
        type: 'user task',
        expressions: {
            proceed: 'user_proceed_request',
            cancel: 'user_reject_request',
        }
    },
    USER_ADD_COMMENT: {
        name: 'User adds comment',
        assignee: 'user',
        type: 'user task',
    },
    USER_CANCELS_REQUEST: {                                       // Need subscribe
        name: 'User cancels request',
        topic: 'cancel_user_request',
        type: 'service task',
    },
    DECLINED_BY_LAWYER: {                                          // Need subscribe
        name: 'Declined by lawyer',
        topic: 'declined_by_lawyer',
        type: 'service task',
    },
    DECLINED_AFTER_TIMEOUT: {                                       // Need subscribe
        name: 'Declined after timeout',
        topic: 'declined_after_timeout',
        type: 'service task',
    },
    USER_NOTIFICATION_DECLINED_BY_LAWYER: {                         // Need subscribe
        name: 'User notification declined by lawyer',
        topic: 'user_notification_declined_by_lawyer',
        type: 'service task',
    },
    USER_NOTIFICATION_DECLINED_BY_TIMEOUT: {                        // Need subscribe
        name: 'User notification declined by timeout',
        topic: 'user_notification_declined_by_timeout',
        type: 'service task',
    },
};

module.exports = TASKS;
/*
INITIATE PROCESS                http://localhost:8080/engine-rest/process-definition/key/lawyer_help/start
GET PROCESS DEFINITION          http://localhost:8080/engine-rest/process-instance/{id}
GET TASK IDS                    http://localhost:8080/engine-rest/task?processInstanceId={processInstanceId}
COMPLETE TASK BY ID             http://localhost:8080/engine-rest/task/{id}/complete
 */

/*
http://localhost:8080/camunda/app/cockpit/default/#/dashboard
 */
