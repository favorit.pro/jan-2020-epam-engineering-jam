# Jan-2020-EPAM-Engineering-JAM.

## Toss a coin

Run the app in Docker
1. From 'frontend' directory: npm run build
2. From 'backend' directory: npm run build
3. From root: docker-compose up --build

The app is running on the port 8070

## For LOCAL DEVELOPMENT
1. Install packages for `frontend` and `backend`
2. Go to `camunda` folder, run `docker-compose up`
3. You need a MongoDB, you can use this steps to get it:
    - `docker pull mongo`
    - `docker run -d -p 27017-27019:27017-27019 --name mongodb mongo`
    - connect via Compass or app using connection string: `mongodb://localhost:27017/jamDB`
7. Install `Camunda Modeler` https://camunda.com/download/modeler/
8. Run `Camunda Modeler`, import `camunda/lawyer_help.bpmn` in it, press `Deploy current diagram button`
    - `Name`: lawyer_help
    - `REST Endpoint`: http://localhost:8080/engine-rest
    - `Authentification`: none
9. Press `Deploy`.
10. After that you will be able to go to http://localhost:8080/camunda/app/cockpit/default/#/dashboard - this is control panel
11. Finally we need to start app. Be sure that you have `NODE_ENV` set to `dev` in next commands.
12. Run `backend` using `yarn start:dev`
13. Run `frontend` using `yarn start`
14. Frontend is running on http://localhost:4200
15. Backend is running on http://localhost:8070
16. To login app you need to create a user. You can do it using Postman or analogues.
    - Route: http://localhost:8070/auth/register?
    - Provide headers:
        - `Content-Type`: `application/x-www-form-urlencoded`
    - Body: x-www-form-urlencoded with keys: value
        - `email`: `yourmail@gmail.com`
        - `password`: `yourpassword`
        - `firstName`: `John`
        - `lastName`: `Smith`
    - Send `POST` request. You should get this answer:
    ```
    { "success": true, "message": "user register" }
    ```
17. We can log in! Yahoo! :)
