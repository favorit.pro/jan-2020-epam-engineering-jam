FROM node:12.14.1

ENV WORKDIR /dist

WORKDIR ${WORKDIR}

COPY ./frontend/dist/frontend ./browser

COPY ./backend/dist ./

RUN npm install --prod && \
  npm cache clean --force

CMD npm run start:docker
