import {HttpStatus, Injectable} from '@nestjs/common';
import {Client, logger} from 'camunda-external-task-client-js';
import {ajax} from 'rxjs/ajax';
import {InjectModel} from '@nestjs/mongoose';
import {Model, Schema} from 'mongoose';
import {RequestsService} from '../requests/requests.service';
import {IProcessRequestMap} from '../models/IProcessRequestMap';
import {RequestDto} from '../requests/dbo/Request.dto';
import {Status} from '../models/Status';
import {UsersService} from '../users/users.service';
import {camundaTasks} from './camunda.tasks';
import {map} from 'rxjs/operators';
import {camundaConfig} from '../config';
import {createXHR} from './camunda.helpers';
import {ObjectId} from 'mongoose/lib/types';
import {getDeclinedByLawyerMail, getDeclinedByTimeoutMail, getStatusChangedMail, mail} from '../utils/mailUtils';

@Injectable()
export class CamundaService {
    client: Client;

    constructor(@InjectModel('ProcessRequestMap') private readonly ProcessRequestMapModel: Model<IProcessRequestMap>,
                private readonly usersService: UsersService,
                private readonly requestsService: RequestsService) {
        this.client = new Client({
            baseUrl: camundaConfig.baseUrl,
            use: logger,
            asyncResponseTimeout: 10000,
        });
        this.initiateSubscribes();
    }

    initiateSubscribes() {
        this.client.subscribe(camundaTasks.SUBMIT_REQUEST.topic, async ({task, taskService}) => {
            await taskService.complete(task);
        });
        this.client.subscribe(camundaTasks.USER_CANCELS_REQUEST.topic, async ({task, taskService}) => {
            const model = await this.ProcessRequestMapModel.findOne({processId: task.processInstanceId});
            await this.requestsService.update(model.requestId, {status: Status.DeclinedUser} as RequestDto);
            await taskService.complete(task);
        });
        this.client.subscribe(camundaTasks.DECLINED_BY_LAWYER.topic, async ({task, taskService}) => {
            const model = await this.ProcessRequestMapModel.findOne({processId: task.processInstanceId});
            await this.requestsService.update(model.requestId, {status: Status.DeclinedLawyer} as RequestDto);
            await taskService.complete(task);
        });
        this.client.subscribe(camundaTasks.DECLINED_AFTER_TIMEOUT.topic, async ({task, taskService}) => {
            const model = await this.ProcessRequestMapModel.findOne({processId: task.processInstanceId});
            await this.requestsService.update(model.requestId, {status: Status.OutOfTime} as RequestDto);
            await taskService.complete(task);
        });
        this.client.subscribe(camundaTasks.USER_NOTIFICATION_DECLINED_BY_LAWYER.topic, async ({task, taskService}) => {
            const model = await this.ProcessRequestMapModel.findOne({processId: task.processInstanceId});
            const request = await this.requestsService.findById(ObjectId(model.requestId));
            const user = await this.usersService.findById(request.ownerUser as unknown as Schema.Types.ObjectId);
            const date = new Date();
            mail([user.username], 'State of request | Состояние запроса', getDeclinedByLawyerMail(user.firstName, user.lastName, request.title, date))
                .then(async () => {
                    await taskService.complete(task);
                })
                .catch(async () => {
                    await taskService.handleFailure(task);
                });
            await taskService.complete(task);
        });
        this.client.subscribe(camundaTasks.USER_NOTIFICATION_DECLINED_BY_TIMEOUT.topic, async ({task, taskService}) => {
            const model = await this.ProcessRequestMapModel.findOne({processId: task.processInstanceId});
            const request = await this.requestsService.findById(ObjectId(model.requestId));
            const user = await this.usersService.findById(request.ownerUser as unknown as Schema.Types.ObjectId);
            const date = new Date();

            mail([user.username], 'State of request | Состояние запроса',
                getDeclinedByTimeoutMail(user.firstName, user.lastName, request.title, date))
                .then(async () => {
                    await taskService.complete(task);
                })
                .catch(async () => {
                    await taskService.handleFailure(task);
                });
            await taskService.complete(task);
        });
        this.client.subscribe(camundaTasks.NOTIFY_USER_ABOUT_STATUS_CHANGE.topic, async ({task, taskService}) => {
            const model = await this.ProcessRequestMapModel.findOne({processId: task.processInstanceId});
            await this.requestsService.update(model.requestId, {status: Status.InWorkUser} as RequestDto);
            const request = await this.requestsService.findById(ObjectId(model.requestId));
            const user = await this.usersService.findById(request.ownerUser as unknown as Schema.Types.ObjectId);
            const date = new Date();
            mail([user.username], 'State of request | Состояние запроса', getStatusChangedMail(user.firstName, user.lastName, request.title, date))
                .then(async () => {
                    await taskService.complete(task);
                })
                .catch(async () => {
                    await taskService.handleFailure(task);
                });
            await taskService.complete(task);
        });
    }

    startProcess(user: Schema.Types.ObjectId, requestDto: RequestDto, response) {
        ajax({
            createXHR,
            url: `${camundaConfig.baseUrl}/process-definition/key/lawyer_help/start`,
            crossDomain: true,
            withCredentials: false,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .subscribe(async res => {
                requestDto = {...requestDto, ownerUser: user};
                this.requestsService.create(requestDto).then((request) => {
                    const requestMapModel = new this.ProcessRequestMapModel({
                        processId: res.response.id as Schema.Types.ObjectId,
                        requestId: request._id as Schema.Types.ObjectId,
                    });
                    requestMapModel.save();
                    response.status(HttpStatus.OK).send(request._id);
                });
            }, err => console.error(err));
    }

    async completeTask(requestId: Schema.Types.ObjectId, taskName: string, variables: any) {
        const model = await this.ProcessRequestMapModel.findOne({requestId});
        return await ajax({
            createXHR,
            url: `${camundaConfig.baseUrl}/task?processInstanceId=${model.processId}`,
            crossDomain: true,
            withCredentials: false,
            method: 'GET',
        })
            .pipe(
                map(async res => {
                    const task = res.response[0];
                    await ajax({
                        createXHR,
                        url: `${camundaConfig.baseUrl}/task/${task.id}/complete`,
                        method: 'POST',
                        body: {variables: {...variables}},
                        crossDomain: true,
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    }).toPromise();
                }, err => console.error(err)),
            ).toPromise();
    }

}
