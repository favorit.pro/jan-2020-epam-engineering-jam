import {XMLHttpRequest} from 'xmlhttprequest';

export function createXHR() {
    return new XMLHttpRequest();
}
