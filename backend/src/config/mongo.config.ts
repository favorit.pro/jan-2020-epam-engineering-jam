export const mongoConfig = {
    connectionString: `mongodb://${ process.env.NODE_ENV === 'prod' ? 'mongodb' : 'localhost' }:27017/jamDb`,
};
