export const camundaConfig = {
    baseUrl: `http://${process.env.NODE_ENV === 'prod' ? 'camunda' : 'localhost'}:8080/engine-rest`,
};
