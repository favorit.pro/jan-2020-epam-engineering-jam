import {Controller, Get, Response} from '@nestjs/common';
import {ApiTags} from '@nestjs/swagger';
import {InjectModel} from '@nestjs/mongoose';
import {PassportLocalModel} from "mongoose";
import {ICity} from '../models/ICity';
import {IUser} from '../models/IUser';
import {Role} from '../models/Role';

@Controller('lawyers')
@ApiTags('lawyers')
export class LawyersController {
    constructor(@InjectModel('User') private readonly userModel: PassportLocalModel<IUser>) {}

    @Get()
    public async getAllLawyers(@Response() res) {
        res.json(await this.userModel.find({role: Role.Lawyer}).exec());
    }
}
