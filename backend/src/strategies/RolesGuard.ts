import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {Reflector} from '@nestjs/core';
import {IUser} from '../models/IUser';
import {Role} from '../models/Role';

@Injectable()
export class RolesGuard implements CanActivate {

    constructor(private readonly reflector: Reflector) {
    }

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const roles = this.reflector.get<string[]>('roles', context.getHandler());
        if (!roles) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        const user: IUser = request.user;
        const hasRole = () => roles.includes(user.role);
        if (user) {
            return user.role && hasRole();
        } else if (roles.includes(Role.Guest)) {
            return true;
        }
        return false;
    }
}
