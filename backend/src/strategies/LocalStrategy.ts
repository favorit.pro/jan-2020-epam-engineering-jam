import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {AuthService} from '../auth/auth.service';
import {IUser} from '../models/IUser';
import {PassportLocalModel} from 'mongoose';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService,
                @InjectModel('User') private readonly userModel: PassportLocalModel<IUser>) {
        super({
            usernameField: 'email',
            passwordField: 'password',
        }, userModel.authenticate());
    }
}
