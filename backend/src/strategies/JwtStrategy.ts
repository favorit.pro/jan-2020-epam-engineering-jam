import {Request} from 'express';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {PassportStrategy} from '@nestjs/passport';
import {Strategy} from 'passport-jwt';
import {AuthService} from '../auth/auth.service';
import {JwtPayload} from '../auth/interfaces/jwt-payload.interface';

function cookieExtractor(req) {
    let token = null;
    if (req && req.cookies) {
        token = req.cookies.token;
    }
    return token;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: cookieExtractor,
            secretOrKey: 'ILovePokemon',
            passReqToCallback: true,
        });
    }

    // tslint:disable-next-line:ban-types
    async validate(req: Request, payload: JwtPayload, done: Function) {
        const user = await this.authService.validateUser(payload);
        done(null, user);
    }
}
