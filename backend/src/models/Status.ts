export enum Status {
    Pending = 'Pending',
    InReview = 'In Review',
    DeclinedLawyer = 'Declined By Lawyer',
    DeclinedTimeout = 'Declined By Timeout',
    DeclinedUser = 'Declined By User',
    Success = 'Success',
    OutOfTime = 'Out of time',
    InWorkUser = 'In Work User',
    InWorkLawyer = 'In Work Lawyer',
}
