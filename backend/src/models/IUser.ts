import {PassportLocalDocument, Schema} from 'mongoose';
import {Role} from './Role';
import {ICompany} from './ICompany';

export interface IUser extends PassportLocalDocument {
    readonly firstName: string;
    readonly lastName: string;
    readonly username: string;
    readonly password: string;
    readonly phone: string;
    readonly role: Role;
    readonly office: string;
}
