import {Document} from 'mongoose';

export interface IComment extends Document {
    readonly userId: string;
    readonly message: string;
    readonly date: string;
}
