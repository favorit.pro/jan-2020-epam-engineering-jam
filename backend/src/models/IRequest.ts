import {Schema, Document} from "mongoose";
import {Status} from './Status';
import {IComment} from './IComment';

export interface IRequest extends Document {
    readonly title: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string;
    readonly phone: string;
    readonly office: string;
    readonly description: string;
    readonly assignUser: string;
    readonly ownerUser: string;
    readonly status: Status;
    readonly comments: IComment[];
    readonly attachments: string;
    readonly updateDate: string;
    readonly requestDate: string;
    readonly isBillable: boolean;
    readonly assignUserName: string;
}
