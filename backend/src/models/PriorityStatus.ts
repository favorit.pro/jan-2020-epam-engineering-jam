import {Status} from './Status';

export const PriorityCountsLawyer = {
    [Status.Success]: 1,
    [Status.DeclinedLawyer]: 3,
    [Status.DeclinedUser]: 2,
    [Status.OutOfTime]: 4,
    [Status.DeclinedTimeout]: 4,
    [Status.InWorkUser]: 5,
    [Status.InReview]: 6,
    [Status.InWorkLawyer]: 7,
    [Status.Pending]: 8,
};


export const PriorityCountsUser = {
    [Status.Success]: 1,
    [Status.DeclinedLawyer]: 3,
    [Status.DeclinedUser]: 2,
    [Status.OutOfTime]: 4,
    [Status.DeclinedTimeout]: 4,
    [Status.InWorkUser]: 8,
    [Status.InReview]: 7,
    [Status.InWorkLawyer]: 6,
    [Status.Pending]: 6,
};
