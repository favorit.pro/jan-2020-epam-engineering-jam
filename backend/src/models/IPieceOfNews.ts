import {Document} from 'mongoose';

export interface IPieceOfNews extends Document {
    readonly title: string;
    readonly description: string;
    readonly introduction: string;
    readonly lang: string;
    readonly date: string;
    readonly image: string;
    readonly isMajor: boolean;
}
