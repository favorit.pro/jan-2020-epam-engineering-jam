import {Schema, Document} from "mongoose";

export interface IProcessRequestMap extends Document {
    readonly processId: string;
    readonly requestId: string;
}
