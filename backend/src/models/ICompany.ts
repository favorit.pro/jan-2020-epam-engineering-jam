import {Document, Schema} from 'mongoose';

export interface ICompany extends Document {
    readonly name: string;
    readonly cityId: string;
}
