export enum Role {
    Admin = 'Admin',
    Editor = 'Editor',
    Lawyer = 'Lawyer',
    User = 'User',
    Guest = 'Guest',
}
