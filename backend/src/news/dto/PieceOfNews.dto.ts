import {ApiModelProperty} from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class PieceOfNewsDto {
    @ApiModelProperty()
    readonly _id: number;

    @ApiModelProperty()
    readonly title: string;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly introduction: string;

    @ApiModelProperty()
    readonly lang: string;

    @ApiModelProperty()
    readonly date: string;

    @ApiModelProperty()
    readonly image: any;

    @ApiModelProperty()
    readonly isMajor: boolean;
}
