import {Schema} from 'mongoose';
import {PieceOfNewsDto} from '../dto/PieceOfNews.dto';
import {IPieceOfNews} from '../../models/IPieceOfNews';

export interface INewsService {
    findAll(): Promise<IPieceOfNews[]>;
    findById(ID: Schema.Types.ObjectId): Promise<IPieceOfNews | null>;
    findOne(options: object): Promise<IPieceOfNews | null>;
    create(pieceOfNewsDto: PieceOfNewsDto): Promise<IPieceOfNews>;
    update(ID: number, newValue: PieceOfNewsDto): Promise<IPieceOfNews | null>;
    delete(ID: number): Promise<string>;
}
