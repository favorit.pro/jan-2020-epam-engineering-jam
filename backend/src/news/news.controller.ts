import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    Response,
    UploadedFile,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import {NewsService} from './news.service';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {IPieceOfNews} from '../models/IPieceOfNews';
import {PieceOfNewsDto} from './dto/PieceOfNews.dto';
import {ApiTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {RolesGuard} from '../strategies/RolesGuard';
import {Roles} from '../decorators/role.decorator';
import {Role} from '../models/Role';
import {FileInterceptor} from '@nestjs/platform-express';
import {editFileName, imageFileFilter} from '../requests/requests.controller';
import {diskStorage} from 'multer';

@ApiTags('news')
@Controller('news')
export class NewsController {
    constructor(@InjectModel('PieceOfNews') private readonly catModel: Model<IPieceOfNews>, private readonly newsService: NewsService) {
    }

    @Get()
    public async getAllNews(@Response() res) {
        res.json(await this.newsService.findAll());
    }

    @Get(':id')
    public async getPieceOfNews(@Param() params, @Response() res) {
        res.json(await this.newsService.findById(params.id));
    }

    @Post()
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Roles(Role.Editor, Role.Admin)
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                destination: './browser/files',
                filename: editFileName,
            }),
            fileFilter: imageFileFilter,
        }),
    )
    public async insertNewPieceOfNews(@UploadedFile() file, @Body() pieceOfNews: any, @Response() res) {
        const news = {...JSON.parse(pieceOfNews.data), image: file && file.path} as PieceOfNewsDto;
        res.json(await this.newsService.create(news));
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Roles(Role.Editor, Role.Admin)
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                destination: './browser/files',
                filename: editFileName,
            }),
            fileFilter: imageFileFilter,
        }),
    )
    public async updatePieceOfNews(@UploadedFile() file, @Param() params, @Body() pieceOfNews: any, @Response() res) {
        const newsObject = JSON.parse(pieceOfNews.data);
        delete newsObject.image;
        if (file) {
            newsObject.image = file.path;
        }
        const news = {...newsObject} as PieceOfNewsDto;

        res.json(await this.newsService.update(params.id, news));
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Roles(Role.Editor, Role.Admin)
    public async removePieceOfNews(@Param() params, @Response() res) {
        res.json(await this.newsService.delete(params.id));
    }
}
