import {Module} from '@nestjs/common';
import {NewsController} from './news.controller';
import {NewsService} from './news.service';
import {MongooseModule} from '@nestjs/mongoose';
import {PieceOfNewsSchema} from '../schemas/pieceOfNews.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'PieceOfNews', schema: PieceOfNewsSchema}])],
    controllers: [NewsController],
    providers: [NewsService],
    exports: [NewsService,
        MongooseModule.forFeature([{name: 'PieceOfNews', schema: PieceOfNewsSchema}])],
})
export class NewsModule {
}
