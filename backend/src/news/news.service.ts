import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model, Schema} from "mongoose";
import {PieceOfNewsDto} from './dto/PieceOfNews.dto';
import {IPieceOfNews} from '../models/IPieceOfNews';
import {debug} from "console";
import {INewsService} from './interfaces/INewsService';

@Injectable()
export class NewsService implements INewsService {
    constructor(@InjectModel('PieceOfNews') private readonly pieceOfNewsModel: Model<IPieceOfNews>) {
    }

    async findAll(): Promise<IPieceOfNews[]> {
        return await this.pieceOfNewsModel.find().exec();
    }

    async findOne(options: object): Promise<IPieceOfNews> {
        return await this.pieceOfNewsModel.findOne(options).exec();
    }

    async findById(ID: Schema.Types.ObjectId): Promise<IPieceOfNews> {
        return await this.pieceOfNewsModel.findById(ID).exec();
    }

    async create(pieceOfNewsDto: PieceOfNewsDto): Promise<IPieceOfNews> {
        const date = new Date().toUTCString();
        const pieceOfNews = new this.pieceOfNewsModel({...pieceOfNewsDto, date});
        return await pieceOfNews.save();
    }

    async update(ID: number, newValue: PieceOfNewsDto): Promise<IPieceOfNews> {
        const pieceOfNews = await this.pieceOfNewsModel.findById(ID).exec();

        if (!pieceOfNews._id) {
            debug('user not found');
        }

        await this.pieceOfNewsModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.pieceOfNewsModel.findById(ID).exec();
    }

    async delete(ID: number): Promise<string> {
        try {
            await this.pieceOfNewsModel.findByIdAndRemove(ID).exec();
            return 'The user has been deleted';
        } catch (err) {
            debug(err);
            return 'The user could not be deleted';
        }
    }
}
