import {Controller, Get, Param, Res, Response} from '@nestjs/common';

@Controller()
export class AppController {

  @Get('/browser/files/:imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './browser/files' });
  }
}
