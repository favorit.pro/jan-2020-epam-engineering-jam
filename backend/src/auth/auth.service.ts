import { Injectable } from '@nestjs/common';
import {UsersService} from '../users/users.service';
import {InjectModel} from '@nestjs/mongoose';
import {PassportLocalModel, Schema, Types} from 'mongoose';
import {IUser} from '../models/IUser';
import {JwtPayload} from './interfaces/jwt-payload.interface';
import {RegistrationStatus} from './interfaces/registrationStatus.interface';
import {sign} from 'jsonwebtoken';
import {CreateUserDto} from '../users/dto/createUser.dto';
import {Role} from '../models/Role';

@Injectable()
export class AuthService {
    constructor(private readonly usersService: UsersService,
                @InjectModel('User') private readonly userModel: PassportLocalModel<IUser>) { }

    async register(user: CreateUserDto) {
        let status: RegistrationStatus = { success: true, message: 'userId register' };
        await this.userModel.register(new this.userModel({
            firstName: user.firstName,
            lastName: user.lastName,
            username: user.email,
            role: Role.User,
            office: user.office,
            phone: user.phone,
        }), user.password, (err) => {
            if (err) {
                status = { success: false, message: err };
            }
        });
        return status;
    }

    createToken(user) {
        const expiresIn = 36000000000;
        const accessToken = sign({ firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            role: user.role,
            office: user.office,
            phone: user.phone,
            _id: user._id}, 'ILovePokemon', { expiresIn });
        return {
            expiresIn,
            accessToken,
        };
    }
    async validateUser(payload: JwtPayload): Promise<any> {
        return await this.usersService.findById(payload._id as unknown as Schema.Types.ObjectId);
    }
}
