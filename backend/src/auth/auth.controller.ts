import {Body, Controller, Get, HttpStatus, Post, Request, Response, UseGuards} from '@nestjs/common';
import {AuthService} from './auth.service';
import {UsersService} from '../users/users.service';
import {CreateUserDto} from '../users/dto/createUser.dto';
import {LoginUserDto} from '../users/dto/loginUser.dto';
import {ApiTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService,
                private readonly usersService: UsersService) {

    }

    @Get('login')
    public async recaptchaLogin(@Response() res) {
        res.json({captcha: res.recaptcha});
    }

    @Post('register')
    public async register(@Response() res, @Body() createUserDto: CreateUserDto) {
        const result = await this.authService.register(createUserDto);
        if (!result.success) {
            return res.status(HttpStatus.BAD_REQUEST).json(result);
        }
        return res.status(HttpStatus.OK).json(result);
    }

    @Post('logout')
    @UseGuards(AuthGuard('jwt'))
    public async logout(@Response() res, @Body() createUserDto: CreateUserDto) {
        return res.status(HttpStatus.OK).clearCookie('token');
    }

    @Post('login')
    @UseGuards(AuthGuard('local'))
    public async login(@Request() req, @Response() res, @Body() login: LoginUserDto) {
        return await this.usersService.findOne({username: login.email}).then(user => {
            if (!user) {
                res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                    message: 'User Not Found',
                });
            } else {
                const localUser = {
                    _id: user._id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.username,
                    role: user.role,
                    office: user.office,
                    phone: user.phone,
                } ;
                const token = this.authService.createToken(localUser);
                return res.status(HttpStatus.OK).cookie('token', token.accessToken, {
                    maxAge: token.expiresIn,
                    httpOnly: true,
                }).json({...localUser});
            }
        });
    }
}
