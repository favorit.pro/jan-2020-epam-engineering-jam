import {MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import {JwtStrategy} from '../strategies/JwtStrategy';
import {LocalStrategy} from '../strategies/LocalStrategy';
import {UsersModule} from '../users/users.module';

@Module({
  imports: [UsersModule],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, LocalStrategy],
  exports: [AuthService, JwtStrategy, LocalStrategy],
})
export class AuthModule { }
