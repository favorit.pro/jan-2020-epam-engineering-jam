import {Controller, Get, Response} from '@nestjs/common';
import {ApiTags} from '@nestjs/swagger';
import {InjectModel} from '@nestjs/mongoose';
import {Model, PassportLocalModel} from "mongoose";
import {ICity} from '../models/ICity';

@Controller('companies')
@ApiTags('companies')
export class CompaniesController {
    constructor(@InjectModel('Company') private readonly companiesModel: Model<ICity>) {}

    @Get()
    public async getAllCompanies(@Response() res) {
        res.json(await this.companiesModel.find().exec());
    }
}
