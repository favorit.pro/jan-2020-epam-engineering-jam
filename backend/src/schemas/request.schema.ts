import {Schema} from 'mongoose';
import {CommentSchema} from './comment.schema';

export const RequestSchema = new Schema({
    title: String,
    description: String,
    assignUser:  String,
    ownerUser: String,
    status: String,
    comments: [CommentSchema],
    attachments: String,
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
    office: String,
    updateDate: String,
    requestDate: String,
    isBillable: Boolean,
});
