import {Schema} from 'mongoose';

export const CommentSchema = new Schema({
    userId: String,
    message: String,
    date: String,
});
