import {Schema} from 'mongoose';

export const PieceOfNewsSchema = new Schema({
    title: String,
    description: String,
    introduction: String,
    lang: String,
    date: String,
    image: String,
    isMajor: Boolean,
});
