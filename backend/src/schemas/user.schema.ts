import * as passportLocalMongoose from 'passport-local-mongoose';
import {Schema} from 'mongoose';
import {CompanySchema} from './company.schema';

export const UserSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    phone: String,
    role: String,
    office: String,
});

UserSchema.plugin(passportLocalMongoose);
