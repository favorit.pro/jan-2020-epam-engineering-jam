import {Schema} from 'mongoose';

export const ProcessRequestMap = new Schema({
    processId: String,
    requestId: String,
});
