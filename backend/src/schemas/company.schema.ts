import {Schema} from 'mongoose';

export const CompanySchema = new Schema({
    name: String,
    cityId: String,
});
