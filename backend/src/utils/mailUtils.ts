import {createTestAccount, createTransport} from 'nodemailer';

export const mail = (to: string[], subject: string, body: string) => new Promise(resolve => {
    createTestAccount((err, account) => {
        // create reusable transporter object using the default SMTP transport
        const transporter = createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'lawyers.jam@gmail.com',
                pass: 'fmdlezelsslhmpxp',
            },
        });

        // setup email data with unicode symbols
        const mailOptions = {
            from: `Lawyers Jam <lawyers.jam@gmail.com>`,
            bcc: to.join('; '),
            subject,
            html: body,
            priority: 'high',
        };
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                process.exit(1);
            }

            transporter.close();
            resolve();
        });
    });
});
export const getDeclinedByLawyerMail = (firstName: string, lastName: string, title, date: Date): string =>
    `Dear <b>${firstName} ${lastName}</b>,<br><br>
    Your request ${title} was successfully closed at ${date.toLocaleString()}.<br><br>

    Best regards,<br>
     Belorussian National Bar Association

    Дорогой ${firstName} ${lastName},<br><br>

    Ваш запрос ${title} был успешно закрыт в ${date.toLocaleString()}.<br><br>

    С наилучшими пожеланиями, <br>
    Белорусская Республиканская Коллегия Адвокатов.`;

export const getDeclinedByTimeoutMail = (firstName: string, lastName: string, title, date: Date): string =>
    `Dear <b>${firstName} ${lastName}</b>,<br><br>

    Your request ${title} was declined due to inactivity and closed at ${date.toLocaleString()}.<br><br>

    Best regards, <br>
    Belarusian National Bar Association


    Дорогой ${firstName} ${lastName},<br><br>

    Ваш запрос ${title} был отменен из-за неактивности и закрыт в ${date.toLocaleString()}.<br><br>

    С наилучшими пожеланиями, <br>Белорусская Республиканская Коллегия Адвокатов.`;

export const getStatusChangedMail = (firstName: string, lastName: string, title, date: Date): string =>
    `Dear <b>${firstName} ${lastName}</b>,<br><br>
    In your request ${title} lawyer was added comment at ${date.toLocaleString()}.<br><br>

    Best regards,<br>
     Belorussian National Bar Association

    Дорогой ${firstName} ${lastName},<br><br>

    В Вашем запросе ${title} адвокат добавил новый комментарий в ${date.toLocaleString()}.<br><br>

    С наилучшими пожеланиями, <br>
    Белорусская Республиканская Коллегия Адвокатов.`;
