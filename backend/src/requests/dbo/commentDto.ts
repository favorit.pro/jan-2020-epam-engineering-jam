export interface CommentDto {
    userId: string;
    message: string;
    date: string;
}
