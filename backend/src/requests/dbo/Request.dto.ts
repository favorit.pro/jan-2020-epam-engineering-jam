import {Schema} from 'mongoose';
import {Status} from '../../models/Status';
import {IComment} from '../../models/IComment';
import {ApiModelProperty} from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class RequestDto {
    @ApiModelProperty()
    readonly title: string;
    @ApiModelProperty()
    readonly description: string;
    @ApiModelProperty()
    readonly assignUser: Schema.Types.ObjectId;
    @ApiModelProperty()
    readonly ownerUser: Schema.Types.ObjectId;
    @ApiModelProperty()
    readonly comments: IComment[];
    @ApiModelProperty()
    readonly attachments: any;

    @ApiModelProperty()
    readonly firstName: string;

    @ApiModelProperty()
    readonly lastName: string;

    @ApiModelProperty()
    readonly email: string;

    @ApiModelProperty()
    readonly phone: string;

    @ApiModelProperty()
    readonly office: Schema.Types.ObjectId;

    @ApiModelProperty()
    readonly status: string;

    @ApiModelProperty()
    readonly updateDate: string;

    @ApiModelProperty()
    readonly requestDate: string;

    @ApiModelProperty()
    readonly isBillable: boolean;
}
