import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model, Schema} from 'mongoose';
import {debug} from 'console';
import {IRequest} from '../models/IRequest';
import {RequestDto} from './dbo/Request.dto';
import {IRequestsService} from './interfaces/IRequestsService';
import {Status} from '../models/Status';
import {PriorityCountsLawyer, PriorityCountsUser} from '../models/PriorityStatus';

@Injectable()
export class RequestsService implements IRequestsService {

    constructor(@InjectModel('Request') private readonly requestModel: Model<IRequest>) {
    }

    async findAll(): Promise<IRequest[]> {
        return await this.requestModel.find().exec();
    }

    async findOne(options: object): Promise<IRequest> {
        return await this.requestModel.findOne(options).exec();
    }

    async findById(ID: Schema.Types.ObjectId): Promise<IRequest> {
        return await this.requestModel.findById(ID).exec();
    }

    async create(requestDto: RequestDto): Promise<IRequest> {
        const date = new Date().toUTCString();

        const request = new this.requestModel({
            ...requestDto,
            status: Status.Pending,
            updateDate: date,
            requestDate: date,
        });
        return await request.save();
    }

    async update(ID: string, newValue: RequestDto): Promise<IRequest> {
        const request = await this.requestModel.findById(ID).exec();

        if (!request._id) {
            debug('userId not found');
        }
        await this.requestModel.findByIdAndUpdate(ID, {...newValue, updateDate: new Date().toUTCString()}).exec();
        return await this.requestModel.findById(ID).exec();
    }

    async getAllById(id: string): Promise<IRequest[]> {
        return [
            ...(await this.requestModel.find({assignUser: id}).exec()),
            ...(await this.requestModel.find({assignUser: null}).exec()),
        ].sort(this.requestStatusCompareLawyer);
    }

    async getAllByIdUser(id: string): Promise<IRequest[]> {
        return [...(await this.requestModel.find({ownerUser: id}).exec())].sort(this.requestStatusCompareUser);
    }

    requestStatusCompareLawyer(a: IRequest, b: IRequest) {
        return PriorityCountsLawyer[b.status] - PriorityCountsLawyer[a.status];
    }
    requestStatusCompareUser(a: IRequest, b: IRequest) {
        return PriorityCountsUser[b.status] - PriorityCountsUser[a.status];
    }
}
