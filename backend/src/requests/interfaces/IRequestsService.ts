import {Schema} from 'mongoose';
import {IRequest} from '../../models/IRequest';
import {RequestDto} from '../dbo/Request.dto';

export interface IRequestsService {
    findAll(): Promise<IRequest[]>;
    findById(ID: Schema.Types.ObjectId): Promise<IRequest | null>;
    findOne(options: object): Promise<IRequest | null>;
    create(requestDto: RequestDto): Promise<IRequest>;
    update(ID: string, newValue: RequestDto): Promise<IRequest | null>;
    getAllById(id: string): Promise<IRequest[]>;
    getAllByIdUser(id: string): Promise<IRequest[]>;
}
