import {
    Body,
    Controller,
    Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Request,
    Response,
    UploadedFile,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import {RequestsService} from './requests.service';
import {ApiTags} from '@nestjs/swagger';
import {Status} from '../models/Status';
import {CamundaService} from '../camunda/camunda.service';
import {RequestDto} from './dbo/Request.dto';
import {Roles} from '../decorators/role.decorator';
import {Role} from '../models/Role';
import {ApiModelProperty} from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import {camundaTasks} from '../camunda/camunda.tasks';
import {FileInterceptor} from '@nestjs/platform-express';
import {diskStorage} from 'multer';
import {extname} from 'path';
import {RolesGuard} from '../strategies/RolesGuard';
import {AuthGuard} from '@nestjs/passport';
import {CommentDto} from './dbo/commentDto';
import {IRequest} from '../models/IRequest';

export const imageFileFilter = (req, file, callback) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif|pdf)$/)) {
        return callback(new Error('Only image files are allowed!'), false);
    }
    callback(null, true);
};

export const editFileName = (req, file, callback) => {
    let name = file.originalname.split('.')[0];
    name = name.replace(/ /g, '_');
    const fileExtName = extname(file.originalname);
    const randomName = Array(4)
        .fill(null)
        .map(() => Math.round(Math.random() * 16).toString(16))
        .join('');
    callback(null, `${name}-${randomName}${fileExtName}`);
};

export class ReviewRequestDto {
    @ApiModelProperty()
    readonly isAccept: boolean;
}

export class ChangeRequestDto {
    @ApiModelProperty()
    readonly isComment: boolean;
    @ApiModelProperty()
    readonly comment?: string;
}

export class UserReviewDto {
    @ApiModelProperty()
    readonly isProceed: boolean;
    @ApiModelProperty()
    readonly comment?: string;
}

@ApiTags('requests')
@Controller('requests')
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class RequestsController {
    constructor(private readonly requestsService: RequestsService, private readonly camundaService: CamundaService) {
    }

    @Get('/:id')
    @Roles(Role.User, Role.Lawyer)
    public async insertComment(@Param() params, @Response() res) {
        res.json(await this.requestsService.findById(params.id));
    }

    @Post('/:id/comments')
    @Roles(Role.User, Role.Lawyer)
    public async getRequest(@Body() comment: CommentDto, @Request() req, @Param() params, @Response() res) {
        const request: IRequest = await this.requestsService.findById(params.id);
        switch (request.status) {
            case Status.InWorkUser:
                if (!request.ownerUser.localeCompare(req.user._id)) {
                    await this.completeUserInWorkTask(request._id);
                    await this.completeUserAddCommentTask(request._id);
                    res.json(await this.addCommentRequest(request, comment, Status.InReview));
                } else {
                    res.json(await this.addCommentRequest(request, comment, Status.InWorkUser));
                }
                break;
            case Status.InWorkLawyer:
                if (!request.assignUser.localeCompare(req.user._id)) {
                    await this.completeLawyerInWorkTaskComment(request._id);
                    await this.completeLawyerAddCommentTask(request._id);
                    res.json(await this.addCommentRequest(request, comment, Status.InWorkUser));
                } else {
                    res.status(HttpStatus.BAD_REQUEST).send();
                }
                break;
            case Status.InReview:
                if (!request.ownerUser.localeCompare(req.user._id)) {
                    res.json(await this.addCommentRequest(request, comment, Status.InReview));
                } else {
                    res.status(HttpStatus.BAD_REQUEST).send();
                }
                break;
            default:
                res.status(HttpStatus.BAD_REQUEST).send();
        }
    }

    @Post('/createRequest')
    @Roles(Role.User, Role.Guest)
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                destination: './browser/files',
                filename: editFileName,
            }),
            fileFilter: imageFileFilter,
        }),
    )
    public async createRequest(@UploadedFile() file, @Body() requestDto: any, @Response() res, @Request() req) {
        const request: RequestDto = {...JSON.parse(requestDto.data), attachments: file && file.path, isBillable: false};
        this.camundaService.startProcess(req.user._id, request, res);
    }

    @Put('/:id/acceptRequest')
    @Roles(Role.Lawyer)
    public async acceptRequest(@Param() params, @Response() res) {
        await this.camundaService.completeTask(params.id, camundaTasks.ACCEPT_USERS_REQUEST_BY_LAWYER.name, {});
        res.json(await this.requestsService.update(params.id, {status: Status.InReview} as RequestDto));
    }

    @Put('/:id/reviewRequestAccept')
    @Roles(Role.Lawyer)
    public async reviewRequest(@Request() req, @Param() params, @Response() res) {
        await this.camundaService.completeTask(params.id, camundaTasks.LAWYER_REVIEW_STAGE.name, {
            [camundaTasks.LAWYER_REVIEW_STAGE.expressions.accept]: {value: true},
            [camundaTasks.LAWYER_REVIEW_STAGE.expressions.decline]: {value: false},
        });
        res.json(await this.requestsService.update(params.id, {
            status: Status.InWorkLawyer,
            assignUser: req.user._id,
        } as RequestDto));
    }

    @Put('/:id/reviewRequestDecline')
    @Roles(Role.Lawyer)
    public async declineLawyer(@Param() params, @Request() req, @Response() res) {
        await this.camundaService.completeTask(params.id, camundaTasks.LAWYER_REVIEW_STAGE.name, {
            [camundaTasks.LAWYER_REVIEW_STAGE.expressions.accept]: {value: false},
            [camundaTasks.LAWYER_REVIEW_STAGE.expressions.decline]: {value: true},
        });
        res.json(await this.requestsService.update(params.id, {status: Status.DeclinedLawyer, assignUser: req.user._id} as RequestDto));
    }

    @Put('/:id/contactUser')
    @Roles(Role.Lawyer)
    public async changeRequest(@Param() params, @Body() body: ChangeRequestDto, @Response() res) {
        await this.completeLawyerInWorkTaskContact(params.id);
        await this.camundaService.completeTask(params.id, camundaTasks.RESOLVE_USER_REQUEST.name, {});
        res.json(await this.requestsService.update(params.id, {status: Status.Success} as RequestDto));
    }

    @Put('/:id/cancelUser')
    @Roles(Role.User)
    public async cancelUser(@Param() params, @Body() body: ChangeRequestDto, @Response() res) {
        await this.camundaService.completeTask(params.id, camundaTasks.USER_REVIEWS_DOCUMENT.name, {
            [camundaTasks.USER_REVIEWS_DOCUMENT.expressions.proceed]: {value: false},
            [camundaTasks.USER_REVIEWS_DOCUMENT.expressions.cancel]: {value: true},
        });
        res.json(await this.requestsService.update(params.id, {status: Status.DeclinedUser} as RequestDto));
    }

    @Put('/:id/billable')
    @Roles(Role.Lawyer)
    public async changeBillableStatus(@Param() params, @Body() body: any, @Response() res) {
        res.json(await this.requestsService.update(params.id, {isBillable: body.isBillable} as RequestDto));
    }

    async completeUserInWorkTask(requestId) {
        await this.camundaService.completeTask(requestId, camundaTasks.USER_REVIEWS_DOCUMENT.name, {
            [camundaTasks.USER_REVIEWS_DOCUMENT.expressions.proceed]: {value: true},
            [camundaTasks.USER_REVIEWS_DOCUMENT.expressions.cancel]: {value: false},
        });

    }

    async completeUserAddCommentTask(requestId) {
        await this.camundaService.completeTask(requestId, camundaTasks.USER_ADD_COMMENT.name, {});
    }

    addCommentRequest = async (request: IRequest, comment: CommentDto, status: Status) => await this.requestsService.update(request.id, {
        comments: [...request.comments, {
            message: comment.message,
            userId: comment.userId,
            date: Date().toString(),
        }],
        status,
    } as RequestDto);

    async completeLawyerInWorkTaskComment(requestId) {
        await this.camundaService.completeTask(requestId, camundaTasks.LAWYER_WORK_WITH_REQUEST.name, {
            [camundaTasks.LAWYER_WORK_WITH_REQUEST.expressions.contact]: {value: false},
            [camundaTasks.LAWYER_WORK_WITH_REQUEST.expressions.comment]: {value: true},
        });
    }

    async completeLawyerInWorkTaskContact(requestId) {
        await this.camundaService.completeTask(requestId, camundaTasks.LAWYER_WORK_WITH_REQUEST.name, {
            [camundaTasks.LAWYER_WORK_WITH_REQUEST.expressions.contact]: {value: true},
            [camundaTasks.LAWYER_WORK_WITH_REQUEST.expressions.comment]: {value: false},
        });
    }

    async completeLawyerAddCommentTask(requestId) {
        await this.camundaService.completeTask(requestId, camundaTasks.LAWYER_ADD_COMMENT.name, {});
    }
}
