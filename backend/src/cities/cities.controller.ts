import {Controller, Get, Response} from '@nestjs/common';
import {ApiTags} from '@nestjs/swagger';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from "mongoose";
import {ICity} from '../models/ICity';

@Controller('cities')
@ApiTags('cities')
export class CitiesController {
    constructor(@InjectModel('City') private readonly citiesModel: Model<ICity>) {
    }

    @Get()
    public async getAllCities(@Response() res) {
        res.json(await this.citiesModel.find().exec());
    }
}
