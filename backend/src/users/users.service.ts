import {PassportLocalModel, Schema, Types} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {debug} from 'console';
import {CreateUserDto} from './dto/createUser.dto';
import {IUser} from '../models/IUser';
import {IUsersService} from './interfaces/IUserService';
import {RecaptchaV2} from 'express-recaptcha/dist';

@Injectable()
export class UsersService implements IUsersService {

    constructor(@InjectModel('User') private readonly userModel: PassportLocalModel<IUser>) {
    }

    async findAll(): Promise<IUser[]> {
        return await this.userModel.find().exec();
    }

    async findOne(options: object): Promise<IUser> {
        return await this.userModel.findOne(options).exec();
    }

    async findById(ID: Schema.Types.ObjectId): Promise<IUser> {
        return await this.userModel.findById(ID).exec();
    }

    async create(createUserDto: CreateUserDto): Promise<IUser> {
        const createdUser = new this.userModel(createUserDto);
        return await createdUser.save();
    }

    async update(ID: number, newValue: IUser): Promise<IUser> {
        const user = await this.userModel.findById(ID).exec();

        if (!user._id) {
            debug('userId not found');
        }

        await this.userModel.findByIdAndUpdate(ID, newValue).exec();
        return await this.userModel.findById(ID).exec();
    }

    async delete(ID: number): Promise<string> {
        try {
            await this.userModel.findByIdAndRemove(ID).exec();
            return 'The userId has been deleted';
        } catch (err) {
            debug(err);
            return 'The userId could not be deleted';
        }
    }
}
