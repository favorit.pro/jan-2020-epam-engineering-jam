import { IUser } from '../../models/IUser';
import {CreateUserDto} from '../dto/createUser.dto';
import {Schema, Types} from 'mongoose';

export interface IUsersService {
    findAll(): Promise<IUser[]>;
    findById(ID: Schema.Types.ObjectId): Promise<IUser | null>;
    findOne(options: object): Promise<IUser | null>;
    create(user: CreateUserDto): Promise<IUser>;
    update(ID: number, newValue: IUser): Promise<IUser | null>;
    delete(ID: number): Promise<string>;
}
