import {Controller, Get, HttpStatus, Param, Request, Response, UseGuards} from '@nestjs/common';
import {Roles} from '../decorators/role.decorator';
import {Role} from '../models/Role';
import {RequestsService} from '../requests/requests.service';
import {AuthGuard} from '@nestjs/passport';
import {RolesGuard} from '../strategies/RolesGuard';
import {ApiTags} from '@nestjs/swagger';
import {UsersService} from './users.service';
import {ObjectId} from 'mongoose/lib/types'

@ApiTags('users')
@Controller('users')
@UseGuards(AuthGuard('jwt'), RolesGuard)
export class UsersController {
    constructor(private readonly requestsService: RequestsService, private readonly usersService: UsersService) {
    }

    @Get('/:id')
    @Roles(Role.Lawyer, Role.User)
    public async getUserById(@Param() params, @Response() res, @Request() req) {
        res.status(HttpStatus.OK).json(await this.requestsService.findById(params.id));
    }

    @Get('/:id/lawyerRequests')
    @Roles(Role.Lawyer)
    public async getAllRequestsLawyer(@Param() params, @Response() res, @Request() req) {
        const requests = await this.requestsService.getAllById(params.id);
        Promise.all(requests.map(async request => {
            if (request.assignUser) {
                const lawyer = await this.usersService.findById(ObjectId(request.assignUser));
                return {
                    ...request.toObject(),
                    assignUserName: `${lawyer.firstName} ${lawyer.lastName}`,
                };
            }
            return request;
        })).then(x => {
            res.status(HttpStatus.OK).json(x);
        });
    }

    @Get('/:id/userRequests')
    @Roles(Role.User)
    public async getAllRequestsUser(@Param() params, @Response() res, @Request() req) {
        const requests = await this.requestsService.getAllByIdUser(params.id);
        Promise.all(requests.map(async request => {
            if (request.assignUser) {
                const lawyer = await this.usersService.findById(ObjectId(request.assignUser));
                return {
                    ...request.toObject(),
                    assignUserName: `${lawyer.firstName} ${lawyer.lastName}`,
                };
            }
            return request;
        })).then(x => {
            res.status(HttpStatus.OK).json(x);
        });

    }
}
