import {Module} from '@nestjs/common';
import {UsersController} from './users.controller';
import {UsersService} from './users.service';
import {MongooseModule} from '@nestjs/mongoose';
import {UserSchema} from '../schemas/user.schema';
import {RequestsModule} from '../requests/requests.module';
import {RequestsService} from '../requests/requests.service';
import {RequestSchema} from '../schemas/request.schema';
import {CompanySchema} from '../schemas/company.schema';
import {CitySchema} from '../schemas/city.schema';
import {CommentSchema} from '../schemas/comment.schema';
import {ProcessRequestMap} from '../schemas/processRequestMapper.schema';

@Module({
    imports: [MongooseModule.forFeature([{
        name: 'User',
        schema: UserSchema
    }]),
        MongooseModule.forFeature([{name: 'Company', schema: CompanySchema}]),
        MongooseModule.forFeature([{name: 'City', schema: CitySchema}]),
        MongooseModule.forFeature([{name: 'Comment', schema: CommentSchema}]),
        MongooseModule.forFeature([{name: 'ProcessRequestMap', schema: ProcessRequestMap}]),
        MongooseModule.forFeature([{name: 'Request', schema: RequestSchema}]), RequestsModule],
    controllers: [UsersController],
    providers: [UsersService, RequestsService],
    exports: [UsersService, MongooseModule.forFeature([{
        name: 'User',
        schema: UserSchema
    }]),
        MongooseModule.forFeature([{name: 'Company', schema: CompanySchema}]),
        MongooseModule.forFeature([{name: 'City', schema: CitySchema}]),
        MongooseModule.forFeature([{name: 'Comment', schema: CommentSchema}]),
        MongooseModule.forFeature([{name: 'ProcessRequestMap', schema: ProcessRequestMap}]),
        MongooseModule.forFeature([{name: 'Request', schema: RequestSchema}])],
})
export class UsersModule {
}
