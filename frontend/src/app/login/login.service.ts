import {Injectable} from '@angular/core';
import {Role, SignInPayload, UserPayload} from './login.types';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {User} from './user';

@Injectable()
export class LoginService {
  userKey = 'user';

  constructor(private http: HttpClient) {
  }

  doLogin({name: email, password, captcha: token}: SignInPayload): Observable<any> {
    return this.http.post(`${environment.serverUrl}/auth/login`, {
      email,
      password,
      token
    });
  }

  isAuthorized() {
    return !!localStorage.getItem(this.userKey);
  }

  getCurrentUser(): UserPayload | null {
    const user = localStorage.getItem(this.userKey);
    return user ? JSON.parse(user) as UserPayload : null;
  }

  get token() {
    return localStorage.getItem('token');
  }

  get isAdmin() {
    const user = this.getCurrentUser();

    return user && user.role === Role.Admin;
  }

  get isUser() {
    const user = this.getCurrentUser();

    return user && user.role === Role.User;
  }

  get isEditor() {
    const user = this.getCurrentUser();

    return user && user.role === Role.Editor;
  }

  get isGuest() {
    const user = this.getCurrentUser();

    return user && user.role === Role.Guest || !user;
  }

  get isLawyer() {
    const user = this.getCurrentUser();

    return user && user.role === Role.Lawyer;
  }

  doLogout() {
    /* /auth/logout */
    localStorage.removeItem(this.userKey);
  }

  getUserId(): string | null {
    const user = this.getCurrentUser();

    return user ? user._id : null;
  }
}
