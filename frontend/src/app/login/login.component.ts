import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {LoginService} from './login.service';
import {User} from './user';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  publicKey = '6LfkoNIUAAAAAOkHuDxHVcQEPwkkFQNZHjV7SQ-t';
  captchaKey = '';
  model = new User();
  doLoginSubscription: Subscription;
  errorIsHidden = true;
  @ViewChild('captcha', {static: true}) captchaRef: ElementRef;

  constructor(
    private loginService: LoginService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.doLoginSubscription) {
      this.doLoginSubscription.unsubscribe();
    }
  }

  resolved(key: string) {
    this.captchaKey = key;
  }

  onSubmit() {
    this.doLoginSubscription = this.loginService.doLogin({
      ...this.model,
      captcha: this.captchaKey
    }).pipe(catchError(x => {
      this.errorIsHidden = false;
      return x;
    })).subscribe((user) => {
      localStorage.setItem('user', JSON.stringify(user));
      if (user) {
        this.router.navigate(['/']);
      }
    });
  }

  closeModal() {
    this.router.navigate(['/']);
  }
}
