import {User} from './user';

export interface SignInPayload extends User {
  captcha: string;
}

export enum Role {
  Admin = 'Admin',
  Editor = 'Editor',
  Lawyer = 'Lawyer',
  User = 'User',
  Guest = 'Guest',
}

export interface Company {
  name: string;
  city: string;
}

export interface UserPayload {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone: string;
  role: Role;
  office: Company;
}
