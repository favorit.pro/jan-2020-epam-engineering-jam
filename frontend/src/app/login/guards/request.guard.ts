import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {LoginService} from '../login.service';
import {LawyerGuard} from './lawyer.guard';
import {UserGuard} from './user.guard';

@Injectable({
  providedIn: 'root'
})
export class RequestGuard implements CanActivate {
  constructor(private loginService: LoginService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return new LawyerGuard(this.loginService).canActivate(next, state) || new UserGuard(this.loginService).canActivate(next, state);
  }

}
