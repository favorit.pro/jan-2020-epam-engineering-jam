export class User {
  name: string;
  password: string;

  constructor(props?) {
    this.name = props && props.name || '';
    this.password = props && props.password || '';
  }
}
