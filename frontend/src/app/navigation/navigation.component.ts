import {Component} from '@angular/core';
import {LoginService} from '../login/login.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {TranslateService} from '@ngx-translate/core';
import {CreateRequestComponent} from '../create-request/create-request.component';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  bsModalRef: BsModalRef;

  constructor(
    public user: LoginService,
    private modalService: BsModalService,
    private translateService: TranslateService
  ) {
  }

  openCreateRequestModal() {
    this.bsModalRef = this.modalService.show(CreateRequestComponent, {
      initialState: {},
      class: 'modal-xl'
    });
    this.bsModalRef.content.closeBtnName = this.translateService.instant('modal.close');
  }
}
