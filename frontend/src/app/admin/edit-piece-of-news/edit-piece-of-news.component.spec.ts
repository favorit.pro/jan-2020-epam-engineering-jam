import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPieceOfNewsComponent } from './edit-piece-of-news.component';

describe('EditPieceOfNewsComponent', () => {
  let component: EditPieceOfNewsComponent;
  let fixture: ComponentFixture<EditPieceOfNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPieceOfNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPieceOfNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
