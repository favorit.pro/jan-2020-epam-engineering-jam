import {Component, OnInit} from '@angular/core';
import {NewsItem} from '../../news-item/news-item.model';
import {CreateNewsService} from '../../create-news/create-news.service';
import {Router} from '@angular/router';
import {NewsService} from '../../news/news.service';

@Component({
  selector: 'app-edit-piece-of-news',
  templateUrl: './edit-piece-of-news.component.html',
  styleUrls: ['./edit-piece-of-news.component.scss']
})
export class EditPieceOfNewsComponent implements OnInit {

  model = {
    title: '',
    description: '',
    introduction: '',
    image: '',
    isMajor: false,
  } as NewsItem;
  submitted = false;
  fileToUpload: File = null;

  constructor(
              private router: Router,
              private newsService: NewsService) {
  }

  onFormSubmit() {
    if (this.submitted) {
      const formData: FormData = new FormData();
      if (this.fileToUpload) {
        formData.append('file', this.fileToUpload, this.fileToUpload.name);

      }
      formData.append('data', JSON.stringify(this.model));
      this.newsService.updateNewsItem(this.model._id, formData).subscribe((res) => {
        this.router.navigate(['/']);
      });
    }
  }

  onSubmit() {
    this.submitted = true;
  }

  handleFileInput(files: HTMLInputElement) {
    if (files.value && files.files[0]) {
      this.fileToUpload = files.files[0];
    }
    files.parentElement.querySelector('label').innerText = this.fileToUpload.name;
  }

  ngOnInit() {
    const id = window.location.pathname.split('/').pop();
    this.newsService.getNewsById(id, false).subscribe(x => {
      this.model = x;
      if (this.model.image) {
        this.model.image = this.model.image.split('/').pop();
      }
    });
  }

}
