import { Component, OnInit } from '@angular/core';
import {NewsItem} from '../../news-item/news-item.model';
import {NewsService} from '../../news/news.service';

@Component({
  selector: 'app-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.scss']
})
export class EditNewsComponent implements OnInit {

  items: NewsItem[] = [];
  constructor(private newsService: NewsService) {}
  ngOnInit() {
    this.newsService.getNews().subscribe(news => {
      this.items = news;
    });
  }

}
