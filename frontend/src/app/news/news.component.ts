import { Component, OnInit } from '@angular/core';
import {NewsItem} from '../news-item/news-item.model';
import {NewsService} from './news.service';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  otherNews: NewsItem[] = [];
  majorNews: NewsItem[] = [];

  constructor(private newsService: NewsService) {}

  ngOnInit(): void {
    this.newsService.getNews().subscribe(news => {
      this.otherNews = news.filter(newsItem => !newsItem.isMajor);
      this.majorNews = news.filter(newsItem => newsItem.isMajor);
    });
  }
}
