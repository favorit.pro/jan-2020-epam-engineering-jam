import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {NewsItem} from '../news-item/news-item.model';
import {environment} from '../../environments/environment';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({providedIn: 'root'})
export class NewsService {

  private newsUrl = `${environment.serverUrl}/news`;

  constructor(private http: HttpClient) {
  }

  getNews(needRefactor: boolean = true): Observable<NewsItem[]> {
    return this.http.get<NewsItem[]>(this.newsUrl)
      .pipe(
        map(reqs =>
          reqs.map(req => needRefactor ? this.mapNewsRefactor(req) : this.mapNewsNotRefactor(req))),
        catchError(this.handleError('getNews', []))
      );
  }

  getNewsById(id: string, needRefactor: boolean = true): Observable<NewsItem> {
    const url = `${this.newsUrl}/${id}`;
    return this.http.get<NewsItem>(url)
      .pipe(
        map(req => needRefactor ? this.mapNewsRefactor(req) : this.mapNewsNotRefactor(req)),
        catchError(this.handleError<NewsItem>(`getNewItem id=${id}`))
      );
  }

  updateNewsItem(id, newsItem: any) {
    return this.http.put<NewsItem>(`${this.newsUrl}/${id}`, newsItem)
      .pipe(map(req => this.mapNewsRefactor(req)));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  mapNewsRefactor = (req: NewsItem): NewsItem => ({
    ...req,
    image: req.image && `${environment.serverUrl}/${req.image}`,
    description: req.description.replace(/\n/g, '<br/>'),
    introduction: req.introduction.replace(/\n/g, '<br/>'),
  });
  mapNewsNotRefactor = (req: NewsItem): NewsItem => ({
    ...req,
    image: req.image && `${environment.serverUrl}/${req.image}`,
  })
}
