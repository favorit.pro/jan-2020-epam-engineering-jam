import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  baseUrl = environment.serverUrl;

  constructor(private readonly http: HttpClient) {
  }

  getCities() {
    return this.http.get(`${this.baseUrl}/cities`);
  }

  getCompanies() {
    return this.http.get(`${this.baseUrl}/companies`);
  }

  getLawyers() {
    return this.http.get(`${this.baseUrl}/lawyers`);
  }
}
