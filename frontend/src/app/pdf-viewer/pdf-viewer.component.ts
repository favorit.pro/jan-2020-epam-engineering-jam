import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {fromEvent, Observable, Subscription} from 'rxjs';
import {debounceTime, throttleTime} from 'rxjs/operators';

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.scss']
})
export class PdfViewerComponent {
  @Input() src!: string;
  @Input() page = 1;

  numPages: number;

  constructor() {
  }

  get numPagesIterable() {
    return new Array(this.numPages);
  }

  onLoadComplete(pdf: any) { // sorry, should be PDFDocumentProxy
    this.numPages = pdf.numPages;
  }

  goToPage(nthPage: number) {
    if (nthPage > this.numPages || nthPage < 0) {
      return;
    }
    this.page = nthPage;
  }
}
