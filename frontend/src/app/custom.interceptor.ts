import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {LoginService} from './login/login.service';
import {environment} from '../environments/environment';

@Injectable()
export class CustomInterceptor implements HttpInterceptor {

  constructor(private loginService: LoginService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      withCredentials: true,
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': environment.serverUrl,
        'Authorization': `Bearer ${this.loginService.token}`
      })
    });

    return next.handle(request);
  }
}
