import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {CreateRequestComponent} from './create-request/create-request.component';
import {ViewRequestsComponent} from './view-requests/view-requests.component';
import {NewsComponent} from './news/news.component';
import {EditRolesComponent} from './admin/edit-roles/edit-roles.component';
import {EditNewsComponent} from './admin/edit-news/edit-news.component';
import {GuestGuard} from './login/guards/guest.guard';
import {AdminGuard} from './login/guards/admin.guard';
import {HelpComponent} from './help/help.component';
import {CreateNewsComponent} from './create-news/create-news.component';
import {EditorGuard} from './login/guards/editor.guard';
import {CreateRequestGuard} from './login/guards/create-request.guard';
import {ReadRequestsGuard} from './login/guards/read-requests.guard';
import {ContactsComponent} from './contacts/contacts.component';
import {NewsItemComponent} from './news-item/news-item.component';
import {LegalAidComponent} from './legal-aid/legal-aid.component';
import {ViewRequestComponent} from './view-requests/view-request/view-request.component';
import {EditPieceOfNewsComponent} from './admin/edit-piece-of-news/edit-piece-of-news.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: NewsComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [
      GuestGuard
    ]
  },
  {
    path: 'request',
    component: CreateRequestComponent,
    canActivate: [
      CreateRequestGuard
    ]
  },
  {
    path: 'requests',
    component: ViewRequestsComponent,
    canActivate: [
      ReadRequestsGuard
    ]
  },
  {
    path: 'news-edit',
    pathMatch: 'full',
    component: EditNewsComponent,
    canActivate: [
      EditorGuard
    ]
  },
  {
    path: 'news-edit/:id',
    pathMatch: 'full',
    component: EditPieceOfNewsComponent,
    canActivate: [
      EditorGuard
    ]
  },
  {
    path: 'admin/roles/edit',
    component: EditRolesComponent,
    canActivate: [
      AdminGuard
    ]
  },
  {
    path: 'news',
    component: NewsComponent,
    pathMatch: 'full'
  },
  {
    path: 'news/:id',
    component: NewsItemComponent,
    pathMatch: 'full'
  },
  {
    path: 'requests/:id',
    component: ViewRequestComponent,
    pathMatch: 'full'
  },
  {
    path: 'help',
    component: HelpComponent
  },
  {
    path: 'news-create',
    component: CreateNewsComponent,
    pathMatch: 'full',
    canActivate: [
      EditorGuard
    ]
  },
  {
    path: 'contacts',
    component: ContactsComponent
  },
  {
    path: 'legal-aid',
    component: LegalAidComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
