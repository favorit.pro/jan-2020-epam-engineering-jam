import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login/login.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {NavigationComponent} from './navigation/navigation.component';
import {HomeComponent} from './home/home.component';
import {PdfViewerComponent} from './pdf-viewer/pdf-viewer.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {RecaptchaModule} from 'ng-recaptcha';
import {CreateRequestComponent} from './create-request/create-request.component';
import {ViewRequestsComponent} from './view-requests/view-requests.component';
import {NewsComponent} from './news/news.component';
import {EditNewsComponent} from './admin/edit-news/edit-news.component';
import {EditRolesComponent} from './admin/edit-roles/edit-roles.component';
import {FormsModule} from '@angular/forms';
import {HelpComponent} from './help/help.component';
import {CreateNewsComponent} from './create-news/create-news.component';
import {LoginService} from './login/login.service';
import {ContactsComponent} from './contacts/contacts.component';
import {RequestPreviewComponent} from './view-requests/request-preview/request-preview.component';
import {MatTooltipModule} from '@angular/material';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {ModalModule} from 'ngx-bootstrap/modal';
import {ViewRequestComponent} from './view-requests/view-request/view-request.component';
import {CarouselModule} from 'ngx-bootstrap/carousel';
import {ChatComponent} from './view-requests/view-request/chat/chat.component';
import {MyMsgComponent} from './view-requests/view-request/chat/my-msg/my-msg.component';
import {PartnerMsgComponent} from './view-requests/view-request/chat/partner-msg/partner-msg.component';
import {NewsItemComponent} from './news-item/news-item.component';
import {LawyerSelectorComponent} from './create-request/lawyer-selector/lawyer-selector.component';
import {LegalAidComponent} from './legal-aid/legal-aid.component';
import {CustomInterceptor} from './custom.interceptor';
import { StatusBarComponent } from './status-bar/status-bar.component';
import { MsgComponent } from './view-requests/view-request/chat/msg/msg.component';
import { DialogComponent } from './view-requests/view-request/chat/dialog/dialog.component';
import { MsgWriterComponent } from './view-requests/view-request/chat/msg-writer/msg-writer.component';
import { HSeparatorComponent } from './view-requests/view-request/h-separator/h-separator.component';
import { RelatedDocsComponent } from './view-requests/view-request/related-docs/related-docs.component';
import { ButtonControlsComponent } from './view-requests/button-controls/button-controls.component';
import { EditPieceOfNewsComponent } from './admin/edit-piece-of-news/edit-piece-of-news.component';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, '/assets/locale/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
    HomeComponent,
    PdfViewerComponent,
    CreateRequestComponent,
    ViewRequestsComponent,
    NewsComponent,
    EditNewsComponent,
    EditRolesComponent,
    HelpComponent,
    CreateNewsComponent,
    ContactsComponent,
    RequestPreviewComponent,
    ViewRequestComponent,
    ChatComponent,
    MyMsgComponent,
    PartnerMsgComponent,
    NewsItemComponent,
    LegalAidComponent,
    LawyerSelectorComponent,
    StatusBarComponent,
    MsgComponent,
    DialogComponent,
    MsgWriterComponent,
    HSeparatorComponent,
    RelatedDocsComponent,
    ButtonControlsComponent,
    EditPieceOfNewsComponent
  ],
  imports: [
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    PdfViewerModule,
    RecaptchaModule,
    FormsModule,
    MatTooltipModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    CarouselModule.forRoot()
  ],
  providers: [HttpClient, LoginService, {
    provide: HTTP_INTERCEPTORS,
    useClass: CustomInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent],
  entryComponents: [ViewRequestComponent],
})
export class AppModule {
}
