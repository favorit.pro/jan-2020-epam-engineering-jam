import {Component, OnInit} from '@angular/core';
import {NewsItem} from '../news-item/news-item.model';
import {CreateNewsService} from './create-news.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.scss']
})
export class CreateNewsComponent implements OnInit {
  model = {
    title: '',
    description: '',
    introduction: '',
    image: '',
    isMajor: false,
  } as NewsItem;
  submitted = false;
  fileToUpload: File = null;

  constructor(private createNewsService: CreateNewsService,
              private router: Router) {
  }

  onFormSubmit() {
    if (this.submitted) {
      const formData: FormData = new FormData();
      if (this.fileToUpload) {
        formData.append('file', this.fileToUpload, this.fileToUpload.name);

      }
      formData.append('data', JSON.stringify(this.model));
      this.createNewsService.doCreateNewsItem(formData).subscribe((res) => {
        this.router.navigate(['/']);
      });
    }
  }

  onSubmit() {
    this.submitted = true;
  }

  handleFileInput(files: HTMLInputElement) {
    if (files.value && files.files[0]) {
      this.fileToUpload = files.files[0];
    }
    files.parentElement.querySelector('label').innerText = this.fileToUpload.name;
  }

  ngOnInit() {
  }

}
