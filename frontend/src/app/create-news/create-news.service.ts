import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CreateNewsService {
  baseUrl = `${environment.serverUrl}/news`;
  constructor(private http: HttpClient) {
  }

  doCreateNewsItem(model: any) {
    return this.http.post(`${this.baseUrl}`, model);
  }
}
