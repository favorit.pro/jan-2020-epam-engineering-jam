import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {RequestStatusVisible} from '../view-requests/request.types';

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.scss']
})
export class StatusBarComponent implements OnChanges {
  @Input() initialStatus: string;

  constructor() {
  }

  public statuses: Array<{ status: string, type: string }> = [];
  public statusElementIndex = 0;
  public classname = '';

  ngOnChanges() {
    this.statuses = Object.values(RequestStatusVisible).map(status => {
      switch (status) {
        case RequestStatusVisible.Declined:
        case RequestStatusVisible.OutOfTime:
          return {status, type: 'error'};
        case RequestStatusVisible.Success:
          return {status, type: 'success'};
        default:
          return {status, type: 'step'};
      }

    });

    this.statusElementIndex = this.statuses.findIndex((element) => element.status === this.initialStatus);
    let initialStatusElement = this.statuses[this.statusElementIndex];

    if (initialStatusElement.type === 'error' || initialStatusElement.type === 'success') {
      this.statuses = this.statuses.filter(status => status.type === 'step' || status.status === this.initialStatus);
      this.statusElementIndex = this.statuses.length - 1;
      initialStatusElement = this.statuses[this.statusElementIndex];
    }

    switch (initialStatusElement.type) {
      case 'error':
        this.classname = 'is-active-error';
        break;
      case 'success':
        this.classname = 'is-active';
        break;
      default:
        this.classname = 'is-active-pending';
    }
  }
}
