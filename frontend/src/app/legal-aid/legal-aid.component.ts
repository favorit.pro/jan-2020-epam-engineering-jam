import { Component, OnInit } from '@angular/core';
import {CreateRequestComponent} from '../create-request/create-request.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-legal-aid',
  templateUrl: './legal-aid.component.html',
  styleUrls: ['./legal-aid.component.scss']
})
export class LegalAidComponent implements OnInit {
  bsModalRef: BsModalRef;
  constructor(private modalService: BsModalService, public translateService: TranslateService) { }

  ngOnInit() {
  }

  openCreateRequestModal() {
    this.bsModalRef = this.modalService.show(CreateRequestComponent, {
      initialState: {},
      class: 'modal-xl'
    });
    this.bsModalRef.content.closeBtnName = this.translateService.instant('modal.close');
  }
}
