import {Component, Input, OnInit} from '@angular/core';
import {Request, RequestStatusColorMapper} from '../request.types';
import {LoginService} from '../../login/login.service';

@Component({
  selector: 'app-request-preview',
  templateUrl: './request-preview.component.html',
  styleUrls: ['./request-preview.component.scss']
})
export class RequestPreviewComponent implements OnInit {
  @Input() request: Request;

  constructor(public loginService: LoginService) {
  }

  ngOnInit() {
  }

  get statusColor() {
    return RequestStatusColorMapper[this.request.status];
  }

}
