import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Request, RequestStatus} from '../request.types';
import {LoginService} from '../../login/login.service';
import {RequestsService} from '../requests.service';

@Component({
  selector: 'app-button-controls',
  templateUrl: './button-controls.component.html',
  styleUrls: ['./button-controls.component.scss']
})
export class ButtonControlsComponent implements OnInit {

  @Input() request: Request;
  public statuses = RequestStatus;
  @Output() requestChange = new EventEmitter();

  constructor(public loginService: LoginService, private requestsService: RequestsService) {
  }

  ngOnInit() {
  }

  acceptRequestLawyer() {
    this.requestsService.acceptRequestInWork(this.request._id).subscribe(x => this.requestChange.emit(x));
  }

  declineRequest() {
    this.requestsService.declineRequest(this.request._id).subscribe(x => this.requestChange.emit(x));
  }

  call() {
    this.requestsService.callLawyer(this.request._id).subscribe(x => this.requestChange.emit());
  }
}
