import {Injectable} from '@angular/core';
import {LoginService} from '../login/login.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Request, RequestStatus} from './request.types';
import {map} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  constructor(
    private http: HttpClient,
    private loginService: LoginService,
    private translateService: TranslateService
  ) {
  }

  getUserRequests(): Observable<Request[]> {
    if (this.loginService.isUser) {
      const userId = this.loginService.getUserId();

      return this.http.get<Request[]>(`${environment.serverUrl}/users/${userId}/userRequests`).pipe(map(reqs =>
        reqs.map(req => this.mapRequest(req))));
    }
  }

  getRequest(id: string): Observable<Request> {
    if (this.loginService.isUser || this.loginService.isLawyer) {
      return this.http.get<Request>(`${environment.serverUrl}/requests/${id}`).pipe(map(req => this.mapRequest(req)));
    }
  }

  getLawyerRequests(): Observable<Request[]> {
    if (this.loginService.isLawyer) {
      const userId = this.loginService.getUserId();

      return this.http.get<Request[]>(`${environment.serverUrl}/users/${userId}/lawyerRequests`).pipe(map(reqs =>
        reqs.map(req => this.mapRequest(req))));
    }
  }

  mapRequest = (req: Request): Request => ({
    ...req,
    attachments: req.attachments && `${environment.serverUrl}/${req.attachments}`,
    requestDate: new Date(req.requestDate).toLocaleString(this.translateService.currentLang),
    updateDate: new Date(req.updateDate).toLocaleString(this.translateService.currentLang),
    description: req.description.replace(/\n/g, '<br/>'),
  });

  checkStatus = (req: Request) => {
    if (req.status === RequestStatus.Pending && this.loginService.isLawyer) {
      this.acceptRequest(req._id);
      return true;
    }
    return false;
  };

  private acceptRequest(id) {
    return this.http.put<Request>(`${environment.serverUrl}/requests/${id}/acceptRequest`, null)
      .pipe(map(req => this.mapRequest(req))).subscribe();
  }

  acceptRequestInWork = (requestId: string) =>
    this.http.put<Request>(`${environment.serverUrl}/requests/${requestId}/reviewRequestAccept`, null)
      .pipe(map(req => this.mapRequest(req)))


  declineRequest = (requestId: string) =>
    this.loginService.isLawyer ?
      this.http.put<Request>(`${environment.serverUrl}/requests/${requestId}/reviewRequestDecline`, null)
        .pipe(map(req => this.mapRequest(req))) :
      this.http.put<Request>(`${environment.serverUrl}/requests/${requestId}/cancelUser`, null)
        .pipe(map(req => this.mapRequest(req)))


  callLawyer = (requestId: string) => this.http.put<Request>(`${environment.serverUrl}/requests/${requestId}/contactUser`, null)
    .pipe(map(req => this.mapRequest(req)))


  updateBillableStatus = (isBillable: boolean, requestId: string) => {
    if (this.loginService.isLawyer) {
      return this.http.put<Request>(`${environment.serverUrl}/requests/${requestId}/billable`, {isBillable})
        .pipe(map(req => this.mapRequest(req)))
    }
  }
}
