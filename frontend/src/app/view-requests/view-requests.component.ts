import {Component, OnDestroy, OnInit} from '@angular/core';
import {Request} from './request.types';
import {TranslateService} from '@ngx-translate/core';
import {RequestsService} from './requests.service';
import {LoginService} from '../login/login.service';
import {Subject, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {map, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-view-requests',
  templateUrl: './view-requests.component.html',
  styleUrls: ['./view-requests.component.scss']
})
export class ViewRequestsComponent implements OnInit, OnDestroy {
  requests: Request[] = [];
  requestsSubscription: Subscription;
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private translateService: TranslateService,
    private requestsService: RequestsService,
    private loginService: LoginService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    if (this.loginService.isUser) {
      this.requestsSubscription = this.requestsService.getUserRequests().subscribe((requests) => {
        this.requests = requests;
      });
    } else if (this.loginService.isLawyer) {
      this.requestsSubscription = this.requestsService.getLawyerRequests().subscribe((requests) => {
        this.requests = requests;
      });
    }
  }

  openRequest(id: string) {
    this.router.navigate(['/requests', id]);
  }

  ngOnDestroy(): void {
    if (this.requestsSubscription) {
      this.requestsSubscription.unsubscribe();
    }
    this.destroy$.next(true);
  }
}
