import {Comment} from './view-request/chat/chat.types';

export interface Request {
  _id: string;
  firstName: string;
  lastName: string;
  phone?: string;
  email?: string;
  title: string;
  description: string;
  requestDate: string;
  updateDate: string;
  attachments?: string;
  status: RequestStatus;
  ownerUser?: string;
  assignUser?: string;
  comments?: Comment[];
  isBillable?: boolean;
  assignUserName?: string;
}

export enum RequestStatus {
  Pending = 'Pending',
  InReview = 'In Review',
  DeclinedLawyer = 'Declined By Lawyer',
  DeclinedTimeout = 'Declined By Timeout',
  DeclinedUser = 'Declined By User',
  Success = 'Success',
  OutOfTime = 'Out of time',
  InWorkUser = 'In Work User',
  InWorkLawyer = 'In Work Lawyer',
}

export enum RequestStatusVisible {
  Pending = 'Pending',
  InReview = 'In Review',
  InWork = 'In Work',
  Success = 'Success',
  Declined = 'Declined',
  OutOfTime = 'Out of time',
}

export const RequestStatusColorMapper = {
  [RequestStatus.Pending]: 'secondary',
  [RequestStatus.InReview]: 'warning',
  [RequestStatus.DeclinedLawyer]: 'danger',
  [RequestStatus.DeclinedTimeout]: 'danger',
  [RequestStatus.DeclinedUser]: 'success',
  [RequestStatus.Success]: 'success',
  [RequestStatus.OutOfTime]: 'info',
  [RequestStatus.InWorkLawyer]: 'primary',
  [RequestStatus.InWorkUser]: 'primary',

};

export interface City {
  _id: string;
  name: string;
}

export interface Company {
  _id: string;
  name: string;
  cityId: string;
}

export interface Lawyer {
  _id: string;
  firstName: string;
  lastName: string;
  companyId: string;
}
