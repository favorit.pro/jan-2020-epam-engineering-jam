import {Component, Input} from '@angular/core';
import {Comment, MessageSide} from '../chat.types';

@Component({
  selector: 'app-partner-msg',
  templateUrl: './partner-msg.component.html',
  styleUrls: ['./partner-msg.component.scss']
})
export class PartnerMsgComponent {
  @Input() comment: Comment;
  @Input() isLawyer: boolean;
  side = MessageSide.Left;

  constructor() {
  }
}
