import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MsgWriterService} from './msg-writer.service';
import {LoginService} from '../../../../login/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-msg-writer',
  templateUrl: './msg-writer.component.html',
  styleUrls: ['./msg-writer.component.scss']
})
export class MsgWriterComponent implements OnInit {
  @Input() isDisabled: boolean;
  @Output() messageSent = new EventEmitter();

  constructor(
    private msgWriterService: MsgWriterService,
    private loginService: LoginService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  onKeyPress(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.sendMessage((event.target as HTMLInputElement).value);
    }
  }

  sendMessage(message: string) {
    console.log(`Sending message: ${message}`);

    const userId = this.loginService.getUserId();
    const requestId = this.router.url.split('/')[2];

    this.msgWriterService.postMessage(message, requestId, userId).subscribe((r) => {
      this.messageSent.emit(r);
    });
  }
}
