import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MsgWriterService {
  baseUrl = environment.serverUrl;

  constructor(
    private http: HttpClient
  ) {
  }

  postMessage(message: string, requestId: string, userId: string) {
    const commentsUrl = `${this.baseUrl}/requests/${requestId}/comments`;

    return this.http.post(commentsUrl, {
      message,
      userId
    });
  }
}
