export interface Comment {
  userId: string;
  message: string;
  date: string;
}

export enum MessageSide {
  Left = 'left',
  Right = 'right'
}
