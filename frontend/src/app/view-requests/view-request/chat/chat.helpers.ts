export function differenceInMinutes(date: Date) {
  const diff = (new Date().getTime() - date.getTime()) / 1000 / 60;
  return Math.abs(Math.round(diff));
}
