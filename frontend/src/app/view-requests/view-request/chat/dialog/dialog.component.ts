import {Component, Input, OnInit} from '@angular/core';
import {Comment} from '../chat.types';
import {LoginService} from '../../../../login/login.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  @Input() comments: Comment[];
  @Input() userId: string;

  constructor(public loginService: LoginService) {
  }

  ngOnInit() {
  }

}
