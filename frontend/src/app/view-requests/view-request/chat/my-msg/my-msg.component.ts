import {Component, Input} from '@angular/core';
import {Comment, MessageSide} from '../chat.types';

@Component({
  selector: 'app-my-msg',
  templateUrl: './my-msg.component.html',
  styleUrls: ['./my-msg.component.scss']
})
export class MyMsgComponent {
  @Input() comment: Comment;

  side = MessageSide.Right;

  constructor() {
  }
}
