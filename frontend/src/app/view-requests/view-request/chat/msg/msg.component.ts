import {Component, Input, OnInit} from '@angular/core';
import {differenceInMinutes} from '../chat.helpers';
import {Comment, MessageSide} from '../chat.types';

@Component({
  selector: 'app-msg',
  templateUrl: './msg.component.html',
  styleUrls: ['./msg.component.scss']
})
export class MsgComponent implements OnInit {
  @Input() comment: Comment;
  @Input() side: MessageSide;
  public isYou: boolean;
  public time: number;
  @Input() isLawyer = false;

  constructor() {
  }

  ngOnInit() {
    this.time = differenceInMinutes(new Date(this.comment.date));
    this.isYou = this.side === MessageSide.Right;
  }

  get placementClassName() {
    return `justify-content-${this.side === MessageSide.Left ? 'start' : 'end'}`;
  }
}
