import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoginService} from '../../../login/login.service';
import {Comment} from './chat.types';
import {Request} from '../../request.types';
import {RequestsService} from '../../requests.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  @Input() comments: Comment[];
  @Input() isDisabled: boolean;
  @Input() request: Request;
  @Output() requestChange: EventEmitter<Request> = new EventEmitter<Request>();
  userId: string;

  constructor(private loginService: LoginService, private requestsService: RequestsService) {
  }

  ngOnInit() {
    this.userId = this.loginService.getUserId();
  }

  messageReceivedInner(req: Request) {
    this.requestChange.emit(this.requestsService.mapRequest(req));
  }
}
