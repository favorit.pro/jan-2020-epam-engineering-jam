import {Component, Input, OnInit} from '@angular/core';
import {Request} from '../../request.types';

@Component({
  selector: 'app-related-docs',
  templateUrl: './related-docs.component.html',
  styleUrls: ['./related-docs.component.scss']
})
export class RelatedDocsComponent implements OnInit {
  @Input() request!: Request;

  constructor() {
  }

  ngOnInit() {
  }

}
