import {AfterViewInit, Component, ElementRef, OnChanges, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Request, RequestStatus, RequestStatusVisible} from '../request.types';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {RequestsService} from '../requests.service';
import {map, switchMap, tap} from 'rxjs/operators';
import {LoginService} from '../../login/login.service';
import {Subject, Subscription} from 'rxjs';

@Component({
  selector: 'app-view-request',
  templateUrl: './view-request.component.html',
  styleUrls: ['./view-request.component.scss']
})
export class ViewRequestComponent implements OnInit, OnChanges {
  request: Request;
  isChatDisabled: boolean;
  isSanded = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private requestsService: RequestsService,
              public loginService: LoginService) {
  }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.requestsService.getRequest(params.get('id'))),
      map(req => {
        if (!this.isSanded) {
          this.isSanded = true;
          if (this.requestsService.checkStatus(req)) {
            req.status = RequestStatus.InReview;
          }
        }
        return req;
      }),
      tap(x => this.isChatDisabled = this.checkForDisabling(x))
    ).subscribe(x => this.request = x);
  }

  mapStatus(status: RequestStatus): string {
    switch (status) {
      case RequestStatus.DeclinedLawyer:
      case RequestStatus.DeclinedTimeout:
        return RequestStatusVisible.Declined;
      case RequestStatus.InWorkLawyer:
      case RequestStatus.InWorkUser:
        return RequestStatusVisible.InWork;
      case RequestStatus.DeclinedUser:
        return RequestStatusVisible.Success;
      default:
        return status;
    }
  }

  private checkForDisabling(request: Request) {
    if (this.loginService.isUser) {
      return !(request.status === RequestStatus.InWorkUser || request.status === RequestStatus.InReview);
    }
    if (this.loginService.isLawyer) {
      return !(request.status === RequestStatus.InWorkLawyer || request.status === RequestStatus.InWorkUser);
    }
  }

  ngOnChanges(): void {
    this.isChatDisabled = this.checkForDisabling(this.request);
  }
  buttonClicked(request: Request): void {
    this.request = request;
    this.isChatDisabled = this.checkForDisabling(this.request);
  }
  isBillableChanged(event) {
    this.requestsService.updateBillableStatus(event.target.checked, this.request._id).subscribe();
  }
}
