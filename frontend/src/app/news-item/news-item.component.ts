import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {NewsItem} from './news-item.model';
import {NewsService} from '../news/news.service';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.scss']
})
export class NewsItemComponent implements OnInit {
  newsItem: NewsItem | void;

  constructor(
    private route: ActivatedRoute,
    private newsService: NewsService,
    private location: Location
  ) {
  }

  ngOnInit(): void {
    const id = window.location.pathname.split('/').pop();
    this.newsService.getNewsById(id).subscribe(x => this.newsItem = x);
  }

  goBack(): void {
    this.location.back();
  }
}
