export class NewsItem {
  // tslint:disable-next-line:variable-name
  _id: number;
  title: string;
  description: string;
  introduction: string;
  date: string;
  image: string;
  isMajor: boolean;
}
