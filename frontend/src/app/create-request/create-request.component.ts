import {Component, OnDestroy, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {UserRequest, UserRequestProps} from './user-request.types';
import {CreateRequestService} from './create-request.service';
import {Subscription} from 'rxjs';
import {LoginService} from '../login/login.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.scss']
})
export class CreateRequestComponent implements OnInit, OnDestroy {
  fileToUpload: File = null;
  model = new UserRequest({
    title: '',
    description: '',
    firstName: '',
    email: '',
    phone: '',
  } as UserRequestProps);
  createRequestSubscription: Subscription;
  submitted = false;
  public isDisabledUserInfo: boolean;

  constructor(
    public bsModalRef: BsModalRef,
    private createRequestService: CreateRequestService,
    private loginService: LoginService,
    private router: Router
  ) {
  }

  ngOnInit() {
    if (this.loginService.isUser) {
      const user = this.loginService.getCurrentUser();

      this.model.email = user.email;
      this.model.firstName = user.firstName;
      this.model.lastName = user.lastName;
      this.model.phone = user.phone;
    }
    this.isDisabledUserInfo = this.loginService.isAuthorized();
  }

  onLawyerChange(lawyerId: string) {
    this.model.assignUser = lawyerId || null;
  }

  onFormSubmit() {
    if (this.submitted) {
      const formData: FormData = new FormData();
      if (this.fileToUpload) {
        formData.append('file', this.fileToUpload, this.fileToUpload.name);

      }
      formData.append('data', JSON.stringify(this.model));
      this.createRequestSubscription = this.createRequestService.doCreateRequest(formData).subscribe((res) => {
        this.router.navigate(['/']);
        this.bsModalRef.hide();
      });
    }
  }

  onSubmit() {
    this.submitted = true;
  }

  handleFileInput(files: HTMLInputElement) {
    if (files.value && files.files[0]) {
      this.fileToUpload = files.files[0];
    }
    files.parentElement.querySelector('label').innerText = this.fileToUpload.name;
  }

  isEmailOrPhoneExists(email, phone) {
    return email || phone;
  }

  ngOnDestroy(): void {
    if (this.createRequestSubscription) {
      this.createRequestSubscription.unsubscribe();
    }
  }
}
