import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {City, Company, Lawyer} from '../../view-requests/request.types';
import {forkJoin, Subject} from 'rxjs';
import {AppService} from '../../app.service';
import {takeUntil} from 'rxjs/operators';

type LawyerId = string;

@Component({
  selector: 'app-lawyer-selector',
  templateUrl: './lawyer-selector.component.html',
  styleUrls: ['./lawyer-selector.component.scss']
})
export class LawyerSelectorComponent implements OnInit, OnDestroy, OnChanges {
  @Input() requestId: string = null;
  @Input() inline = false;

  @Input() set selectedLawyer(value: string) {
    this._selectedLawyer = value;
  }

  _selectedLawyer: string = null;
  _selectedCity: string = null;
  _selectedCompany: string = null;
  cities: City[] = [];
  companies: Company[] = [];
  lawyers: Lawyer[] = [];
  standalone = {standalone: true};
  destroy$: Subject<boolean> = new Subject<boolean>();

  @Output() lawyerChange = new EventEmitter<LawyerId>();

  constructor(
    private appService: AppService
  ) {
  }

  ngOnInit(): void {
    const getCities = this.appService.getCities();
    const getCompanies = this.appService.getCompanies();
    const getLawyers = this.appService.getLawyers();

    forkJoin([
      getCities,
      getCompanies,
      getLawyers
    ]).pipe(
      takeUntil(this.destroy$)
    ).subscribe(([cities, companies, lawyers]: [City[], Company[], Lawyer[]]) => {
      this.cities = cities;
      this.companies = companies;
      this.lawyers = lawyers;

      console.log(cities, companies, lawyers, this._selectedCompany, this._selectedLawyer, this._selectedCity);

      if (this.lawyers.length && this._selectedLawyer) {
        const assignLawyer = this.lawyers.find(lawyer => lawyer._id === this._selectedLawyer);
        console.log(assignLawyer);
        this._selectedCompany = assignLawyer ? assignLawyer.companyId : null;
      }

      if (this.companies.length && this._selectedCompany) {
        const assignLawyerCompany = this.companies.find(company => company._id === this._selectedCompany);
        this._selectedCity = assignLawyerCompany ? assignLawyerCompany.cityId : null;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
  }

  get selectedCityCompanies() {
    return this.companies.filter(company => company.cityId === this._selectedCity);
  }

  get selectedCompanyLawyers() {
    return this.lawyers.filter(lawyer => lawyer.companyId === this._selectedCompany);
  }

  onLawyerChange(event: Event) {
    const selectElement = event.target as HTMLElement;
    const isCityChange = selectElement.id.startsWith('view-request-city');
    const isCompanyChange = selectElement.id.startsWith('view-request-company');

    if (isCityChange) {
      this._selectedLawyer = null;
      this._selectedCompany = this.selectedCityCompanies.length ? this.selectedCityCompanies[0]._id : null;
    }

    if (isCompanyChange) {
      this._selectedLawyer = this.selectedCompanyLawyers.length ? this.selectedCompanyLawyers[0]._id : null;
    }

    this.lawyerChange.emit(this._selectedLawyer);
  }
}
