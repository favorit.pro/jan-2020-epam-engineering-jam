import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserRequest} from './user-request.types';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CreateRequestService {
  baseUrl = `${environment.serverUrl}/requests`;
  constructor(private http: HttpClient) {
  }

  doCreateRequest(model: any) {
    return this.http.post(`${this.baseUrl}/createRequest`, model);
  }
}
