export class Comment {
  constructor(
    public userId: number,
    public comment: string,
    public date: string,
  ) {
  }
}

export interface UserRequestProps {
  id?: number;
  title: string;
  description: string;
  firstName: string;
  lastName?: string;
  email?: string;
  phone?: string;
  office?: number;
  assignUser?: string;
  ownerUser?: string;
  comments?: Comment[];
  attachments?: File | FormData | string;
}

export class UserRequest {
  id?: number;
  title: string;
  description: string;
  firstName: string;
  lastName?: string;
  email?: string;
  phone?: string;
  office?: number;
  assignUser?: string;
  ownerUser?: string;
  comments?: Comment[];
  attachments?: File | FormData | string;

  constructor(props: UserRequestProps) {
    this.id = props.id;
    this.title = props.title;
    this.description = props.description;
    this.firstName = props.firstName;
    this.lastName = props.lastName;
    this.email = props.email;
    this.phone = props.phone;
    this.office = props.office;
    this.assignUser = props.assignUser;
    this.ownerUser = props.ownerUser;
    this.comments = props.comments;
    this.attachments = props.attachments;
  }
}
