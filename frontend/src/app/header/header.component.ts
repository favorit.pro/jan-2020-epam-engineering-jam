import {Component, OnInit} from '@angular/core';
import {LoginService} from '../login/login.service';
import {TranslateService} from '@ngx-translate/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {CreateRequestComponent} from '../create-request/create-request.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  bsModalRef: BsModalRef;

  constructor(
    private loginService: LoginService,
    private user: LoginService,
    public translateService: TranslateService,
    private modalService: BsModalService,
  ) {
    translateService.addLangs(['en', 'ru']);
    translateService.setDefaultLang('ru');
  }

  ngOnInit() {
  }

  get isAuthorized() {
    return this.loginService.isAuthorized();
  }

  logout() {
    this.loginService.doLogout();
  }

  openCreateRequestModal() {
    this.bsModalRef = this.modalService.show(CreateRequestComponent, {
      initialState: {},
      class: 'modal-xl'
    });
    this.bsModalRef.content.closeBtnName = this.translateService.instant('modal.close');
  }
}
